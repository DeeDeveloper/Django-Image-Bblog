from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404,redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from .models import Post
from .forms import PostForm

# Create your views here.
def posts_create(request):
	form =PostForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "Item Saved")
		return HttpResponseRedirect(instance.get_absolute_url())
	context = {
	"form":form,
    }
	return render(request,'post_form.html', context)
	
def posts_detail(request, pk=None):
	# if request.user.is_authenticated():
	#    context ={
	#     "title":"My user List "
	#    }
	# else:
	instance= get_object_or_404(Post, id=pk)
	context ={
	"title":instance.title,
	"instance":instance
	 }
	return render(request,'post_detail.html', context)
def posts_list(request):
	queryset_list =Post.objects.all() #.order_by('-timestamp')
	paginator = Paginator(queryset_list, 3) # Show 25 contacts per page

	page = request.GET.get('page')
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)

	context ={
		"obj_list":queryset,
		"title":"List"
	}
	return render(request,'post_list.html', context)
	# return HttpResponse("<h1>Welcome to main page</h1>")


def posts_update(request, pk=None):
	instance = get_object_or_404(Post, id=pk)
	form = PostForm(request.POST or None, request.Files or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "Item Updated")
		return HttpResponseRedirect(instance.get_absolute_url())
	context = {
	    "title":instance.title,
	    "instance":instance,
	    "form":form,
	}
	return render(request, "post_form.html", context)
	
def posts_delete(request, pk=None):
	instance = get_object_or_404(Post, id=pk)
	instance.delete()
	messages.success(request, "Succesfully Deleted")
	return redirect("blogger:list")
	